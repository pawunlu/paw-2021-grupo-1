class PawMenu {
	constructor(pContenedor) {
		
		let pantallaMobile = window.matchMedia("(max-width: 768px)"); //para consultar resolucion de pantalla

		//Se consigue el nodo contenedor
		let contenedor = pContenedor.tagName
			? pContenedor
			: document.querySelector(pContenedor);

		if ( (contenedor) && ( pantallaMobile.matches ) )
		{
            //solo si se consigue el nodo contenedor Y la resolucion de pantalla "es mobile"

            //se agregan las clases
			contenedor.classList.add("PawMenu");
			contenedor.classList.add("PawMenuCerrado");
            
            //se linkea el estilo
			let css = Paw.nuevoElemento("link", "", {
				rel: "stylesheet",
				href: "js/components/styles/PawMenu.css",
			});
			document.head.appendChild(css);

			//Se arma el boton

			let boton = Paw.nuevoElemento("button", "", {
				class: "PawMenuAbrir",
			});

            //se da el comportamiento al boton dependiendo de si el nodo sobre el cual se hizo click estaba en "modo abierto o modo cerrado"
			boton.addEventListener("click", (event) => {
				if (event.target.classList.contains("PawMenuAbrir")) {
					event.target.classList.add("PawMenuCerrar");
					event.target.classList.remove("PawMenuAbrir");
					contenedor.classList.add("PawMenuAbierto");
					contenedor.classList.remove("PawMenuCerrado");
				} else {
					event.target.classList.add("PawMenuAbrir");
					event.target.classList.remove("PawMenuCerrar");
					contenedor.classList.add("PawMenuCerrado");
					contenedor.classList.remove("PawMenuAbierto");
				}
			});

			// Se inserta el boron en el contenedor
			contenedor.prepend(boton);
		} else {
			if (!pantallaMobile.matches)
				console.info("La pantalla no es mobile, no se debe mostrar el menu hamburguesa");
			else
				console.error("Elemento HTML para generar el MENU no encontrado");
		}
	}
}