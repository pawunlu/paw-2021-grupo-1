class PawCarousel{

    // pContenedor = contenedor que tendrá el carousel
    // carpetaImg = path de la carpeta que contiene las imagenes (ejemplo: /imagenes/)
    constructor(pContenedor, carpetaImg){

        let contenedor = pContenedor.tagName
			? pContenedor
			: document.querySelector(pContenedor);
        
        if ( (contenedor) ){

            //se agregan las clases

            //se linkea el estilo

            // Se arman los botones
            let boton_anterior = Paw.nuevoElemento("button", "Prueba Ant", {
                type: "button",
                class: "boton boton_anterior"
            });

            let boton_siguiente = Paw.nuevoElemento("button", "Prueba Sig", {
                type: "button",
                class: "boton boton_siguiente"
            });

            // Se prepara el carousel
            // Primera imagen


            
            contenedor.appendChild(boton_anterior);
            contenedor.appendChild(boton_siguiente);
        }else{
            console.error("Elemento HTML para generar el CAROUSEL no encontrado");
        }

    }// End Constructor


}