// en caso que no tengamos la posibilidad de generar nuevos scrips
// cargariamos a las clases dentro del script principal

class AppPAW {
  constructor() {
    //se Inicializa la funcionalidad Menu

    if (window.matchMedia("(max-width: 768px)") ) { 
      //Se inicializa el menu si y solo si la pantalla no supera la resolusion especificada

    document.addEventListener("DOMContentLoaded", () => {
      Paw.cargarScript("PawMenu", "js/components/PawMenu.js", () => {
        let menu = new PawMenu("nav");
      });
      // Carousel de imagenes
      Paw.cargarScript("PawCarousel", "js/components/PawCarousel.js", () =>{
        // Le pasa un lsitadod e images (en forma de ruta)
        // Path de la carpeta donde estarán las imagenes
        let carousel = new PawCarousel( "main", "/imagenesCarousel/" );
    });

  }

    //Inicializar otra funcionalidad X

    /*PAW.cargarScript("PawMenu", "js/components/PawMenu.js", () => {
                document.addEventListener("DOMContentLoaded", () => {
                    let menu = new PawMenu();
                });
            });*/
  }

}

let App = new AppPAW();