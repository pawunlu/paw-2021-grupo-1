<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/noticia.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>

    <!-- Noticias -->
    <main>
        <!-- El sigueinte nav es de referencia a las pag anteriores-->
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/noticias">Noticias</a></li>
            </ul>
        </nav> 
        <section class="container-Noticia">
            <h2> <?= $noticia['Titulo'] ?> </h2>
            <p> <time> <?= $noticia['Fecha'] ?> </time> </p>
            <figure>
                <img src="<?= $noticia['Imagen_Large'] ?>" alt="imagen de la noticia">
            </figure>
            <p class="texto-Completo">
                <?= $noticia['Texto_Completo'] ?>
            </p>
        </section>

        <section class="container-tel"> <!--seccion de numeros de contacto -->
            <span class="telefonoVerde"></span>
            <h2>Teléfonos</h2>
            <ul>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
            </ul>
        </section>
    </main>
     <!-- Footer -->
     <?php require 'parts/footer.php'; ?>
</body>
</html>
