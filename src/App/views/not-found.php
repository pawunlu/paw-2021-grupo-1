<!DOCTYPE html>
<html lang="es">
<head>
    <title>404 Página no encontrada</title>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/errores.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
    <main>
        <section>
            <h2>¡No encontramos la página solicitada!</h2>
            <p>Por favor revisá el link ingresado</p>
            <p>La página podría no estar disponible temporalmente</p>
            <p>Código de error: 404</p>
            <a href="/" class="button">Ir al Inicio</a>
        </section>
    </main>
    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>