<footer>
        <!-- Logo -->
        <h3 class="logo-link-footer">  
            <a href="/">                            
                <span class="logo"></span>
                UNLuPAW Medical Group
            </a>
        </h3>

        <!-- Direcciones -->
        <address>
            <p>&copy;UNLuPAW Medical Group</p>
            <ul>
                <li>Aviso Legal</li>
                <li>Dirección: Calle Falsa 123</li>
                <li>Correo:<a href="#"> unlupaw@medicalgroup.com</a></li>
            </ul>
        </address> 

       <!-- Social -->
       <nav>
        <ul>
            <li id="iconoF"><a href="https://www.facebook.com"><span class="icon-face"></span></a></li> 
            <li id="iconoT"><a href="https://twitter.com"><span class="icon-Twitter"></span></a></li> 
            <li id="iconoI"><a href="https://www.instagram.com"><span class="icon-Insta"></span></a></li> 
            <li id="iconoL"><a href="https://ar.linkedin.com"><span class="icon-LinkedIn"></span></a></li>
        </ul>
    </nav>

        <!-- Copyright -->
        <p>Autores: Facundo Williams, Angel Gallozo, Sebastian Marchetti.</p>

</footer>