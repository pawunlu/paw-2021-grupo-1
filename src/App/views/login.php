<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/register.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
    <main>
        <?php if ( isset($tipo) ) {
                    require 'parts/mensaje.php';
            }
        ?> 
        <form action="/ingresar" method="POST">
        <label for="email">Ingrese Correo Electrónico:</label>
        <input type="email" id="email" name="Email">
        <label for="password">Ingrese Contraseña:</label>
        <input type="password" id="password" name="Password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
        <input type="submit" value="Ingresar">
        <a href="/registrarse"> Registrarse</a>
        </form>

    </main>
    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>