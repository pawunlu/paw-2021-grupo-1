<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/noticias.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>

    <!-- Noticias -->
    <main>
        <!-- El sigueinte nav es de referencia a las pag anteriores-->
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/noticias">Noticias</a></li>
            </ul>
        </nav> 
        <h2>Noticias</h2>
        <section class="container-noticias">
            <h4>Noticias</h4>
                <?php $i = 0; while($i <= 5 && isset($noticias[$i])){ ?>
                    <article>
                        <h3> <?= $noticias[$i]['Titulo'] ?> </h3>
                        <figure>
                            <img src="<?= $noticias[$i]['Imagen_Small'] ?>" alt="imagen generica">
                        </figure>
                        <p>  <?= $noticias[$i]['Descripcion'] ?> </p>
                        <a href="/noticia?id=<?= $noticias[$i]['ID_Noticia'] ?>"> ver más...</a>
                    </article>
                <?php $i++; } ?>
            
           
            <!-- <article> 
                <h3>Titulo</h3>
                <figure>
                    <img src="/imagenes/chicaTomandoTe.jpg" alt="imagen de vacuna covid19">
                </figure>
                <p >Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit magnam cupiditate eum eius dolorum, deserunt unde aspernatur similique architecto provident non minus nam reiciendis ut obcaecati dolore ea repudiandae tenet...</p>
                <a href="/html/noticiaGenerica.html"> ver más...</a>
            </article> -->
    
        </section>
        
        <a href="#" class="btn-more-notices"> Ver más noticias</a>

        <section class="container-tel"> <!--seccion de numeros de contacto -->
            <span class="telefonoVerde"></span>
            <h2>Teléfonos</h2>
            <ul>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
            </ul>
        </section>
    </main>
     <!-- Footer -->
     <?php require 'parts/footer.php'; ?>
</body>
</html>
