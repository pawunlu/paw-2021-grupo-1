<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/profes-esp.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
     <!-- El sigueinte nav es de referencia a las pag anteriores-->
     
     
     <!-- Profesionales y especialidades -->
     <main class="container-profesional">
        <nav>
            <ul>
                <li><a href="/index.html">Home</a></li>
                <li><a href="/html/profesionales-especialidades.html">Especialidades/Profesionales</a></li>
                <li><a href="/html/profesional.html">Profesional</a></li>
            </ul>
        </nav> 
        <section>
            <section class="container-prof profesional">
                <img src="/imagenes/doctor.svg" alt="foto-del-profesional">
                <h4> <?= $prof['Name'] ?> </h4>
                <p> <?= $prof['DescripcionCargo'] ?> </p>
            </section>
            <section class="info-profesional os-atiende">
                <h4>Obras Sociales que atiende</h4>
                <ul>
                    <?php foreach($obrasS as $obra): ?>
                        <li> <?= $obra['NameOS'] ?> </li>
                    <?php endforeach ?>
                </ul>
            </section>
            <section class="info-profesional horarios">
                <h4>Horarios</h4>
                <ul>
                    <?php foreach($Horarios as $horario): ?>
                        <li> <?= $horario['Dia'] ?>: <?= $horario['Hora_Inicio'] ?> hs a <?= $horario['Hora_Fin'] ?>hs </li>
                    <?php endforeach ?>
                </ul>
                <a class="button btn-especialidad" href="/nuevo-turno?id_prof=<?php echo $ID_Prof; ?>">Sacar Turno</a>
            </section> 
        </section>
   </main>
    

    <!-- Footer -->
    <?php require 'parts/footer.php';?>
</body>
</html>