<?php 
 namespace Paw\App\Controllers;

 use Paw\Core\Controller;
 use Paw\App\Models\Profesional;
 use Paw\App\Models\ProfesionalCollection;
 use Paw\App\Models\ObraProfCollection;
 use Paw\App\Models\ObraSocialCollection;

 class PageController extends Controller{
         
    // setear en getProf y getEsp
    public ?string $modelName = null;

    /**
     * Funcion que devuelve la pagina Home.
     *
     * @return void
     */
    public function home(){
        $title = 'UNLuPAW Medical Group';
        require $this->viewsDir . 'home.php';
     }
         
    /**
     * Funcion que devuelve la pagina Listado de Turnos
     *
     * @return void
     */
    public function listado_turnos(){
        $title = 'Listado de Turnos';
        require $this->viewsDir . 'listado-turnos.php';
    }    
   
    /**
     * Funcion que devuelve la pagina Institucional
     *
     * @return void
     */
    public function institucional(){
        $title = 'Institucional';
        require $this->viewsDir . 'institucional.php';
    }     
    /**
     * Funcion que devuelve la pagina Noticias
     *
     * @return void
     */
    public function noticias(){
        $title = 'Noticias';
        require $this->viewsDir . 'noticias.php';
    }
    
    /**
     * Procesa el Turno Cargado via POST
     *
     * @return void
     */
    public function procesarTurno(){
        $formulario = $_POST;
        $this->nuevo_turno();
    }
    
    /**
     * Busca Especialidades, Profesionales y Obras Sociales.
     *
     * @return void
     */
    public function buscar(){
        $formulario = $_POST;
        $this->home();
    }    
    
    /**
     * Muestra la pagina de Registro.
     *
     * @return void
     */
    public function registrarse(){
        $title = "Registrarse";
        require $this->viewsDir . 'register.php';
    }
    
    /**
     * Muestra la pagina de Ingreso.
     *
     * @return void
     */
    public function ingresar(){
        $title = "Ingresar";
        require $this->viewsDir . 'login.php';
    }
 }