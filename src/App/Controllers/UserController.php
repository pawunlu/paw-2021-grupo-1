<?php 

namespace Paw\App\Controllers;

use Paw\Core\Controller;
use Paw\App\Models\UsuariosCollection;

class UserController extends Controller{

    
    public ?string $modelName =  UsuariosCollection::class;

    public function set(){

    }

    public function edit(){

    }

    public function get(){

    }

    public function allUsers(){
        $title = "Usuarios";
        $users= $this->model->getAll();
        require $this->viewsDir . 'users.php';
    }

    public function registrarUsuario(){
        $title = "Ingresar";
        $page_redirect = $this->viewsDir . 'register.php'; 
        $titulo = "Error al Registrar";
        $tipo   = 2;
        $descripcion = "Combinacion de Correo y Contraseña no válido";
        if ( isset($_POST['Password']) && filter_var( $_POST['Email'], FILTER_VALIDATE_EMAIL ) && $this->passwordInputValid($_POST['Password']) ) {
            $find_user = $this->model->exists($_POST['Email']);
            if ( 0 === count($find_user) ){
                $email    = $_POST['Email'];
                $password = $_POST['Password'];
                $password_hash = password_hash($_POST['Password'], PASSWORD_BCRYPT);
                $this->model->insert($_POST['Email'],$password_hash);
                $title = 'UNLuPAW Medical Group';
                $tipo  = 1;
                $titulo = "Usuario Registrado con Exito.";
                $descripcion = "";
                $page_redirect =  $this->viewsDir . 'home.php'; 
            }else{
                // Provisional.
                $descripcion = "Correo electrónico ya Existe.";
            }
        }else{
            $descripcion ="Valores No validos";
        }
        require $page_redirect;
    }

    public function ingresarUsuario(){
        $title = "Ingresar";
        $page_redirect = $this->viewsDir . 'login.php'; 
        $titulo = "Error de Ingreso";
        $tipo   = 2;
        $descripcion = "Combinacion de Correo y Contraseña no válido";
        if ( isset($_POST['Password']) && filter_var( $_POST['Email'], FILTER_VALIDATE_EMAIL ) ) {
            if ( $this->passwordInputValid($_POST['Password']) ) {
                $find_user = $this->model->exists($_POST['Email']);
                if( 0 < count($find_user) ){
                    $ver_pass = password_verify( $_POST['Password'], current($find_user)['Password']);
                    if ( $ver_pass ){
                        // Provisional.
                        $title = 'UNLuPAW Medical Group';
                        $titulo = "";
                        $tipo  = null;
                        $page_redirect =  $this->viewsDir . 'home.php';    
                    }
                }else{
                    $descripcion = "Correo electronico NO registrado.";
                }
            }else{
                $descripcion = "Contraseña NO válida.";
            }
            
        }
        require $page_redirect;
    }

    public function passwordInputValid( $password ){
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);

        return ( $uppercase && $lowercase && $number && strlen($password) >= 8 );
    }
}