<?php 

namespace Paw\App\Controllers;

use Paw\Core\Controller;
use Paw\App\Models\ProfesionalCollection;
use Paw\App\Models\ObraProfCollection;

use Paw\App\Models\HorarioCollection;
use Paw\App\Models\Horario_ProfCollection;

use Paw\Core\Database\QueryBuilder;

class Obra_ProfController extends Controller{

    // model = profesionales
    // modelOS_P = Obra_prof
    public ?string $modelName = ProfesionalCollection::class;
    public $modelOS_P;

    public $modelHora;
    public $modelHora_P;

    public function __construct(){
        global $connection, $log;
        parent::__construct();
        $qb = new QueryBuilder($connection);
        $qb->setLogger($log);
        $this->modelOS_P = new ObraProfCollection;
        $this->modelOS_P->setQueryBuilder($qb);

        $this->modelHora = new HorarioCollection;
        $this->modelHora->setQueryBuilder($qb);

        $this->modelHora_P = new Horario_ProfCollection;
        $this->modelHora_P->setQueryBuilder($qb);

    }

    public function set(){

    }

    public function edit(){

    }

    public function get(){

    }

    /**
     * Buscar Profesional por su ID
     *
     * @return void
     */
    // funcion para conseguir la vista de un solo profesional
    public function getProf(){
        $title = "Profesional";
        global $request;
        // Obtener al profesional
        $ID_Prof = $request->get('id');
        
        $prof = $this->model->getProfID($ID_Prof);
        // Obtener las obras sociales del profesional
        $obrasS = $this->modelOS_P->getOS_ProfID($ID_Prof);

        // Obtener Horarios del Profesional
        $HorariosProf = $this->modelHora_P->getHorario_ProfID($ID_Prof);
        
        // Obtener Horas Ini y Fin del horario
        $Horarios = [];
        foreach($HorariosProf as $HoraP){
            $HorarioSpecific = current($this->modelHora->getHorariosID($HoraP->fields['NameDia'],$HoraP->fields['Hora_Inicio'],$HoraP->fields['Hora_Fin']));
            $auxiliar = array_merge(array('Dia' => $HoraP->fields['NameDia']), $HorarioSpecific->fields);
            $Horarios[] = $auxiliar;
        }
        require $this->viewsDir . 'profesional.php';
    }

}