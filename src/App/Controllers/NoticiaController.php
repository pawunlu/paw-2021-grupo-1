<?php 

namespace Paw\App\Controllers;

use Paw\Core\Controller;
use Paw\App\Models\NoticiaCollection;

class NoticiaController extends Controller{

    public ?string $modelName = NoticiaCollection::class;

    public function set(){

    }

    public function edit(){

    }

    public function get(){

    }

    /**
     * Funcion que devuelve la pagina Especialidades y Profesionales
     *
     * @return void
     */
    public function noticias(){
        // Setear modelo profesional
        $title = 'Noticias';
        $noticias = $this->model->getAll();
        require $this->viewsDir . 'noticias.php';
    }    

    public function getNoticia(){
        $title = 'Noticias';
        $ID_Noticia = $_GET['id'];
        $noticia = $this->model->getbuscar($ID_Noticia);
        require $this->viewsDir . 'noticia.php';
    }
    


}