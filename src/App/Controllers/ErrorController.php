<?php 

    namespace Paw\App\Controllers;

    use Paw\Core\Controller;

    class  ErrorController extends Controller{

        public function notFound(){
            http_response_code(404); // Forma de que php le comunica al webserver que aca deberia ir un error 404.
            $title = 'UNLuPAW Medical Group';
            require $this->viewsDir . 'not-found.php';
        }

        public function internalError(){
            $title = 'UNLuPAW Medical Group';
            http_response_code(500); // Forma de que php le comunica al webserver que aca deberia ir un error 404.
            require $this->viewsDir . 'internal-error.php';
        }

    }
 
