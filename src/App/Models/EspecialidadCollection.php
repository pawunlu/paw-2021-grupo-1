<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\Especialidad;

class EspecialidadCollection extends Model{

    public $table = 'Especialidades';

    public function getAll(){
        // Crear tantos Autor como filas d ela tabla autores
        $especialidades = $this->queryBuilder->select($this->table);    
        $espsCollection = [];
        foreach($especialidades as $esp){
            $newEsp = new Especialidad;
            $newEsp->set($esp);
            $espsCollection[] = $newEsp->fields;
        }
        return $espsCollection;
    }

    // Devuelve una lista cuyos nombres matcheen lo que recibe
    public function getBuscar($conditions){
        $especialidades = $this->queryBuilder->select($this->table, $conditions);
        $espsCollection = [];
        foreach($especialidades as $esp){
            $newEsp = new Especialidad;
            $newEsp->set($esp);
            $espsCollection[] = $newEsp->fields;
        }
        return $espsCollection;
    }

    // devuelve una especialidad buscada por su IDE
    public function getEspID($id_Esp){
        $esp = new Especialidad;
        $esp->setQueryBuilder($this->queryBuilder);
        $esp->load($id_Esp);
        return $esp->fields;
    }
    // devuelve una Especialidad buscada por un array de condiciones.
    public function getEspecialidad($condiciones){
        $esp = new Especialidad;
        $especialidad =$this->queryBuilder->select($this->table,$condiciones);
        $esp->set(current($especialidad));
        return $esp->fields;
    }

}