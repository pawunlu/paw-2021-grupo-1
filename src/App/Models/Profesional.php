<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class Profesional extends Model{

    public $table = 'Profesionales';

    public $fields = [
        "ID_Prof"       => null,
        "Name" => null,
        "Email"    => null,
        "DescripcionCargo" => null,
    ];

    
    public function setName( string $prof_name){
        if ( strlen( $prof_name ) > 60 ) {
            throw new InvalidFormatValueException("El nombre del Autor no debe superar los 60 caracteres.");
        }
        $this->fields['Name'] = $prof_name;
    }
    public function setEmail( string $prof_email){
        if ( ! filter_var( $prof_email, FILTER_VALIDATE_EMAIL ) ) {
            throw new InvalidFormatValueException("Formato del Email NO Valido.");
        }
        $this->fields['Email'] = $prof_email;
    }
    
    public function setDescripcionCargo(string $desc){
        $this->fields['DescripcionCargo'] = $desc;
    }

    public function setId_prof( $usr_id){
        $this->fields['ID_Prof'] = $usr_id;
    }
  
    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }
            $method = "set" . ucfirst($field);
            if (method_exists($this,$method)){
                $this->$method( $values[$field] );
            }
        }
    }

    // recupera un profesional de la BD y carga sus datos en la instancia
    public function load($id){
        // Obtener el profesional
        $params = [ ['and' => [ ['ID_Prof','=', $id ] ] ] ];
        $record = current($this->queryBuilder->select($this->table, $params));
        $this->set($record);
    }

}