<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\EspProf;

class EspProfCollection extends Model{

    public $table = 'Esp_Prof';

    public function getAll(){
        $esp_prof = $this->queryBuilder->select($this->table);    
        $E_P_Collection = [];
        foreach($esp_prof as $E_P){
            $newE_P = new EspProf;
            $newE_P->set($E_P);
            $E_P_Collection[] = $newE_P->fields;
        }
        return $E_P_Collection;
    }

    // retorna las esp_Prof por el ID de una especialidad
    public function getEsp_ProfID($IDE){
        $params = [ ['and' => [ ['ID_Esp','=',$IDE ] ] ] ];
        $E_P_Collection_select = $this->queryBuilder->select($this->table, $params);  
        $E_P_Collection = [];
        foreach($E_P_Collection_select as $E_P){
            $newE_P = new EspProf;
            $newE_P->set($E_P);
            $E_P_Collection[] = $newE_P->fields;
        }
        return $E_P_Collection;
    }

    public function getProfAsociados($espID){
        $profesionales = $this->queryBuilder->joinProf_EspProf($espID);
        foreach($profesionales as $prof){
            $newProf = new Profesional;
            $newProf->set($prof);
            $prof_collection[] = $newProf->fields;
        }

        return $prof_collection;
    }

    public function getEspAsociados($profID){
        $especialidades = $this->queryBuilder->joinEsp_EspProf($profID);
        foreach($especialidades as $esp){
            $newEsp = new Especialidad;
            $newEsp->set($esp);
            $esp_collection[] = $newEsp->fields;
        }
        return $esp_collection;
    }
    
}