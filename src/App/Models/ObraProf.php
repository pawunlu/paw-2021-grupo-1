<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class ObraProf extends Model{

    public $table = 'Obra_Prof';

    public $fields = [
        "NameOS" => null,
        "ID_Prof" => null,
    ];

    
    public function setNameos( string $nameOS){
        if ( strlen( $nameOS ) > 60 ) {
            throw new InvalidFormatValueException("El nombre del Autor no debe superar los 60 caracteres.");
        }
        $this->fields['NameOS'] = $nameOS;
    }

    public function setId_prof( $ID_Prof){
        $this->fields['ID_Prof'] = $ID_Prof;
    }
  
    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }


}