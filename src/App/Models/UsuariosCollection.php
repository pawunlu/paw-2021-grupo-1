<?php

namespace Paw\App\Models;

use Paw\Core\Model;

use Paw\App\Models\Usuario;

class UsuariosCollection extends Model{ 
    
    public $table = 'Usuarios';

    public function getAll(){
        //Crear usuarios en base a cantidad en bd
        $users = $this->queryBuilder->select($this->table);
        $usersCollection = [];
        foreach ($users as $user){
            $newUser = new Usuario;
            $newUser->set($user);
            $usersCollection[] = $newUser;
        }
        return $usersCollection;
    }
    
    public function exists( $email ){
        $conditions = [ ['and' => [ ['Email','=',$email ] ] ] ];
        return $this->queryBuilder->select($this->table,$conditions);
    }

    public function insert($email, $pass){
        $insert_fields = [ 
            'Email'    => $email,
            'Password' => $pass,
        ];
        $this->queryBuilder->insert($this->table, $insert_fields);
    }
}