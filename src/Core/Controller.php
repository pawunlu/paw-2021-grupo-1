<?php 

namespace Paw\Core;

use Paw\Core\Model;
use Paw\Core\Database\QueryBuilder;

class Controller{
    public string $viewsDir; // Se crea porque en cada path se repiten las vistas.

    public ?string $modelName = null; 

    public function __construct(){
        global $connection, $log;

        $this->viewsDir = __DIR__ . "/../App/views/";
         
         //Agregamos el menu Dinámico.
         $this->menu = [
            [
                'href' => '/listado-turnos',
                'name' => 'Listado de Turnos',
            ],
            [
                'href' => '/obras-sociales',
                'name' => 'Obras Sociales',
            ],
            [
                'href' => '/especialidades-profesionales',
                'name' => 'Profesionales y Especialidades',
            ],
            [
                'href' => '/noticias',
                'name' => 'Noticias',
            ],
            [
                'href' => '/institucional',
                'name' => 'Institucional',
            ],
        ];

        if (! is_null($this->modelName)) {
            $qb = new QueryBuilder($connection);
            $qb->setLogger($log);
            $model = new $this->modelName;
            $model->setQueryBuilder($qb);
            $this->setModel( $model);

        }
    }

    public function setModel( $model ){
        $this->model = $model;
    }

}